﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class WoodController : MonoBehaviour
{

    public HammerView HammerView;
    public GameObject Woodobje;


    public IEnumerator WoodCreate(bool isInitial)
    {
        if(!isInitial)
            yield return new WaitForSeconds(Random.Range(2f,3f));
        
        Formation formation = Instantiate(Woodobje, transform.position, Quaternion.identity).gameObject.GetComponent<Formation>();
        formation.objeSpeed = Random.Range(10f,12f);
        
        formation.CreateNails(Random.Range(1,3));
        StartCoroutine(WoodCreate(false));
    }
}
