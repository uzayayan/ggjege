﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public TMP_Text ScoreText;
    public Image HealthText;
    public HammerView Hammer;
    public WoodController WoodController;
    public GameObject StartButton;
    public int MaxMissingNails = 10;

    private int missingNails;
    public int MissingNails
    {
        get { return missingNails; }
        set
        {
            missingNails = value;
            
            if(!isEnd)
                CheckMissingNails();
        }
    }

    private bool isEnd = false;

    public void CheckMissingNails()
    {
        HealthText.fillAmount = 1f - ((float) missingNails / MaxMissingNails);
        if (missingNails >= MaxMissingNails)
        {
            isEnd = true;
            WoodController.StopAllCoroutines();
            DestroyAllFormation();

            StartButton.GetComponent<BoxCollider>().enabled = true;
            StartButton.transform.DOMove(new Vector3(-1.9f, -1.94f, 17), 1).OnComplete(() =>
            {
                missingNails = 0;
                HealthText.fillAmount = 1;
                Hammer.score = 0;
                ScoreText.text = "0";
                isEnd = false;
            });
        }
    }

    public void DestroyAllFormation()
    {
        Formation[] formations = GameObject.FindObjectsOfType<Formation>();

        foreach (var formation in formations)
        {
            Destroy(formation.gameObject);   
        }
    }
}

