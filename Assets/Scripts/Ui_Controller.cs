﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ui_Controller : MonoBehaviour

{
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("MainMenü");
        }
    }

    public void StartGame()
    {
        SceneManager.LoadScene("Test");
    }
    public void ExitGame()
    {
        Application.Quit();
        Debug.Log("CIKILDI");
    }

    public void MainMenüBack()
    {
        SceneManager.LoadScene("MainMenü");

    }
}
