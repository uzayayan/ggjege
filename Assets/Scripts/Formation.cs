﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Formation : MonoBehaviour
{
    public GameObject nail;
    public float MinX;
    public float MaxX;
    public float minY;
    public float maxY;
    public float objeSpeed = 3;

    public List<NailView> NailViews = new List<NailView>();

    public void CreateNails(int count)
    {
        for (int i = 0; i < count; i++)
        {
            NailView newNail = Instantiate(nail, Vector3.zero, Quaternion.Euler(0,0,180)).GetComponent<NailView>();
            
            newNail.Initalize(this);
            newNail.transform.SetParent(transform);
            newNail.transform.localPosition = new Vector3(GetRandomAxis(MinX, MaxX), Random.Range(minY, maxY),0);
            
            NailViews.Add(newNail);
        }
    }

    public void Update()
    {
        transform.Translate(transform.right * (Time.deltaTime * objeSpeed));

        if (transform.position.x >= 20)
        {
            Destroy(gameObject);
        }
    }

    public List<float> lastFloatAxis = new List<float>();
    public float GetRandomAxis(float min, float max)     
    {
        float generetadAxis = Random.Range(min, max);
        
        while (ScanLastIndexs(generetadAxis))
        {
            generetadAxis = Random.Range(min, max);
        }

        lastFloatAxis.Add(generetadAxis);
        return generetadAxis;
    }

    public bool ScanLastIndexs(float generetadAxis)
    {
        foreach (var lastFloat in lastFloatAxis)
        {
            if (Mathf.Abs(lastFloat - generetadAxis) < 2.5F)
            {
                return true;
            }
        }
        return false;
    }

    public void RemoveNail(NailView nailView)
    {
        NailViews.Remove(nailView);
    }

    public void OnDestroy()
    {
        try
        {
            GameObject.Find("GameController").GetComponent<GameController>().MissingNails += NailViews.Count;
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }
}
