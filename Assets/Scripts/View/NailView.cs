﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NailView : MonoBehaviour
{
    public Formation Formation;
    public bool isActive = true;

    public void Initalize(Formation formation)
    {
        Formation = formation;
    }
    
    public void Impact()
    {
        transform.localPosition = new Vector3(transform.localPosition.x,-1.25F, transform.localPosition.z);
        isActive = false;
        
        Formation.RemoveNail(this);
    }
}
