﻿using DG.Tweening;
using UnityEngine;

public class HammerView : MonoBehaviour
{
    
    public GameController GameController;
    public WoodController WoodController;
    public GameObject ImpactParticle;
    public float HorizontalSensivity = 20;
    public float VerticalSensivity = 2;
    public float singleStep = 0.1F;

    public AudioSource AudioSource;
    public AudioClip HammerSound;
    public AudioClip WoodSound;
    
    public int score;
    
    //First we'll need a couple of variables to do the calculation.
    Vector3 rotationLast; //The value of the rotation at the previous update
    Vector3 rotationDelta; //The difference in rotation between now and the previous update
 
    //Initialize rotationLast in start, or it will cause an error
    void Start() {
        rotationLast = transform.rotation.eulerAngles;
    }
 
    void Update () {
        //Update both variables, so they're accurate every frame.
        rotationDelta = transform.rotation.eulerAngles - rotationLast;
        rotationLast = transform.rotation.eulerAngles;
        
        Debug.Log(angularVelocity);
    }
 
    //Ta daa!
    public Vector3 angularVelocity {
        get {
            return rotationDelta;
        }
    }
    
    public void TransformControl(Vector3 direction)
    {
        if (transform.position.x >= -5 && transform.position.x <= 5)
        {
            transform.Translate(-direction.z * (Time.deltaTime  * HorizontalSensivity),0,0);
        }
        else
        {
            if(transform.position.x < -5)
                transform.position = new Vector3(-5, transform.position.y, transform.position.z);
            
            if(transform.position.x > 5)
                transform.position = new Vector3(5, transform.position.y, transform.position.z);
        }

        transform.rotation = Quaternion.Slerp(transform.rotation,Quaternion.Euler(-direction.x * VerticalSensivity, 0, 0),Time.deltaTime *singleStep);
    }

    private void AddScore()
    {
        score++;
        GameController.ScoreText.text = score.ToString();
    }
    
    private void PlaySound(bool isHammer)
    {
        if(isHammer)
            AudioSource.clip = HammerSound;
        else
            AudioSource.clip = WoodSound;
            
        AudioSource.Play();
    }
    
    public void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Nail")
        {

            NailView nailView = collider.gameObject.GetComponent<NailView>();

            if (nailView.isActive)
            {
                ParticleSystem impactParticle = Instantiate(ImpactParticle,collider.transform.position,Quaternion.identity).gameObject.GetComponent<ParticleSystem>();
                impactParticle.Play();
                
                nailView.Impact();
                AddScore();
                PlaySound(true);
            }
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Start")
        {
            WoodController.StartCoroutine(WoodController.WoodCreate(true));
            collision.gameObject.GetComponent<BoxCollider>().enabled = false;
            collision.gameObject.transform.DOMove(new Vector3(collision.gameObject.transform.position.x, -6F, collision.gameObject.transform.position.z), 1);
            PlaySound(false);
        }
    }
}
