﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HammerMainMenu : MonoBehaviour
{
    public TMP_InputField ServerInputField;
    public TMP_InputField WaitingFor;

    public void OpenLevel(string levelName)
    {
        if(ServerInputField.text == "" || WaitingFor.text == "")
            return;
        
        PlayerPrefs.SetString("IP", ServerInputField.text);
        PlayerPrefs.SetFloat("Wait",float.Parse(WaitingFor.text));
        SceneManager.LoadScene(levelName);
    }
}
